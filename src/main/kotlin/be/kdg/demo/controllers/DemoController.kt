package be.kdg.demo.controllers

import org.springframework.http.*
import org.springframework.web.bind.annotation.*

@RestController
class DemoController {

    @GetMapping
    fun demoMessage(): ResponseEntity<String> {
        val responseHeaders = HttpHeaders()
        responseHeaders["Content-Type"] = "text/plain; charset=ascii"
        return ResponseEntity("Hello, world V2.0!", responseHeaders, HttpStatus.OK)
    }
}
